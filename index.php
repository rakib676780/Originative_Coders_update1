<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mess Online</title>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- css files -->
    <link rel="stylesheet" href="resources/forhome/css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="resources/forhome/css/style.css" type="text/css" media="all" /> <!-- Style-CSS -->
    <link rel="stylesheet" href="resources/forhome/css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
    <!-- //css files -->

    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Cormorant+Garamond:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700" rel="stylesheet">
    <!-- //online-fonts -->

    <!-- js -->
    <script type="text/javascript" src="resources/forhome/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="resources/forhome/js/bootstrap.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap -->
    <!-- //js -->
</head>
<body>
<!-- header -->
<div class="header">
    <div class="agile-top-header">
        <div class="banner-agile-top">
            <div class="number">
                <h3><i class="fa fa-phone" aria-hidden="true"></i> +880 18********</h3>
            </div>
            <div class="top-icons">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="logo">
            <h1><a href="index.php">MESS<span>ONLINE</span></a></h1>
        </div>
        <!-- navigation -->
        <div class="top-left">
            <div class="top-nav">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div>
                    <!-- navbar-header -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <nav class="linkEffects linkHoverEffect_2">
                            <ul>
                                <li><a href="index.php" data-link-alt="Home" class="active"><span>Home</span></a></li>
                                <li><a href="#about" data-link-alt="about" class="scroll"><span>About</span></a></li>
                                <li><a href="#services" data-link-alt="services" class="scroll"><span>Services</span></a></li>
                                <li><a href="#gallery" data-link-alt="gallery" class="scroll"><span>Gallery</span></a></li>
                                <li><a href="#testimonials" data-link-alt="testimonials" class="scroll"><span>Testimonials</span></a></li>
                                <li><a href="#contact" data-link-alt="Contact Us" class="scroll"><span>Contact Us</span></a></li>
                            </ul>
                        </nav>

                    </div>
                </nav>
                <div class="search">
                    <form action="#" method="post">
                        <input type="search" placeholder="Search..." name="Search" required="">
                        <input type="submit" value="">
                    </form>
                </div>
                <div class="clearfix"> </div>
            </div>

        </div>
        <div class="clearfix"> </div>
        <!-- //navigation -->
    </div>
</div>
<!--Slider-->
<div class="slider">
    <div class="callbacks_container">
        <ul class="rslides" id="slider">
            <li>
                <div class="slider-info">
                    <h3>Lorem ipsum</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lectus orci, volutpat at diam et, tincidunt malesuada turpis. In viverra diam turpis, nec porttitor turpis feugiat in. Pellentesque.</p>
                    <div class="readmore-w3">
                        <a class="readmore" href="#" data-toggle="modal" data-target="#myModal1">Read More</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-info1">
                    <h3>adiping elitrs</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lectus orci, volutpat at diam et, tincidunt malesuada turpis. In viverra diam turpis, nec porttitor turpis feugiat in. Pellentesque.</p>
                    <div class="readmore-w3">
                        <a class="readmore" href="#">Read More</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-info2">
                    <h3>incidit utabr</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lectus orci, volutpat at diam et, tincidunt malesuada turpis. In viverra diam turpis, nec porttitor turpis feugiat in. Pellentesque.</p>
                    <div class="readmore-w3">
                        <a class="readmore" href="#">Read More</a>
                    </div>
                </div>
            </li>
        </ul>

    </div>
    <div class="clearfix"></div>
</div>
<!--//Slider-->

<div class="main" id="main">
    <div class="w3layouts_main_grid">
        <div class="booking-form-head-agile">
            <h3> Login To Our Site</h3>
        </div>
        <form role="form" action="view/OriginativeCoders/ums/User/Authentication/login.php" method="post" class="login-form">

            <div class="w3_agileits_main_grid w3l_main_grid">
                <div class="agileits_grid">
                    <h5>E-mail </h5>

                    <input type="text" name="email" placeholder="Email..." id="form-email" required="" >

                </div>

                <div class="agileits_grid">
                    <h5>Password </h5>
                    <input type="text" name="password" placeholder="Password..." id="form-password" required="">

                </div>
                <a href="view/OriginativeCoders/ums/User/Profile/forgotten.php" style="color:white">Forgot Password?</a>

                <div class="w3_main_grid_right">
                    <input type="submit" value="Login">
                </div>
            </div>

            <div class="w3_main_grid">



                <p style="font-size: 20px; color: white"> If your are not registered, <a href="view/OriginativeCoders/ums/User/Profile/signup.php">Please signup</a> </p>

                <div class="clearfix"> </div>
            </div>
        </form>
    </div>

</div>
<!-- //header -->



<!-- js files -->
<!-- For-Banner -->
<script src="resources/forhome/js/responsiveslides.min.js"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            manualControls: '#slider3-pager',
        });
    });
</script>
<!-- //For-Banner -->

<!--//footer-->
<script src="resources/forhome/js/easyResponsiveTabs.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true   // 100% fit in a container
        });
    });
</script>
<!-- //script for portfolio -->
<!-- //For-Gallery-js -->
<!-- for-Testimonials -->
<!-- required-js-files-->
<link href="resources/forhome/css/owl.carousel.css" rel="stylesheet">
<script src="resources/forhome/js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            items : 1,
            lazyLoad : true,
            autoPlay : true,
            navigation : false,
            navigationText :  false,
            pagination : true,
        });
    });
</script>
<!--//required-js-files-->
<!-- //for-Testimonials -->
<!-- Calendar -->
<link rel="stylesheet" href="resources/forhome/css/jquery-ui.css" />
<script src="resources/forhome/js/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#datepicker,#datepicker1" ).datepicker();
    });
</script>
<!-- //Calendar -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="resources/forhome/js/move-top.js"></script>
<script type="text/javascript" src="resources/forhome/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- smooth scrolling-bottom-to-top -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */
        $().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<script src="resources/forhome/js/SmoothScroll.min.js"></script>



</body>
</html>