<?php
namespace App\Mess;
use App\Message\Message;
use App\Model\Database;
use PDO;

class Mess extends Database
{
    public $id, $postId, $userName, $buildingName,$location,$district,$totalSit,$roomNumber,$mobileNumber,$gender,$condition;

    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray))
            $this->id = $postArray["id"];

        if (array_key_exists("postId", $postArray))
            $this->postId = $postArray["postId"];

        if (array_key_exists("userName", $postArray))
            $this->userName = $postArray["userName"];

        if (array_key_exists("buildingName", $postArray))
            $this->buildingName = $postArray["buildingName"];

        if (array_key_exists("location", $postArray))
            $this->location = $postArray["location"];

        if (array_key_exists("district", $postArray))
            $this->district = $postArray["district"];


        if (array_key_exists("totalSit", $postArray))
            $this->totalSit = $postArray["totalSit"];


        if (array_key_exists("roomNumber", $postArray))
            $this->roomNumber = $postArray["roomNumber"];

        if (array_key_exists("mobileNumber", $postArray))
            $this->mobileNumber = $postArray["mobileNumber"];

        if (array_key_exists("gender", $postArray))
            $this->gender = $postArray["gender"];

        if (array_key_exists("condition", $postArray))
            $this->condition = $postArray["condition"];

    }// end of setData Method

    public function store()
    {

    $sqlQuery = " INSERT INTO create_profile ( post_id,user_name,building_name,location,district,sit_available,room_number,mobile_number, typ ,conditi ) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $dataArray = [
                       $this->postId,
                       $this->userName,
                       $this->buildingName,
                       $this->location,
                       $this->district,
                       $this->totalSit,
                       $this->roomNumber,
                       $this->mobileNumber,
                       $this->gender,
                       $this->condition
                       ];
        $sth = $this->dbh->prepare($sqlQuery);
        $status = $sth->execute($dataArray);
        if ($status)

            Message::message("Success! Data has been inserted successfully<br>");

            else

                Message::message("Failed! Data has not been inserted<br>");

    }// end of store() Method



    public function index(){

        $sqlQuery = "Select * from create_profile WHERE is_trashed='NO' ORDER by id desc ";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function delete()
    {

        $sqlQuery = "DELETE FROM create_profile where id=" . $this->id;

        $status = $this->dbh->exec($sqlQuery);


        if ($status)

            Message::message("Success! Data has been deleted successfully<br>");
        else
            Message::message("Failed! Data has not been deleted<br>");


    }//end of delete() Method


    public function view()
    {

        $sqlQuery = " SELECT * FROM create_profile WHERE id= " . $this->id;
        $sth = $this->dbh->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $oneData = $sth->fetch();
        return $oneData;

    }// end of view() Method





}