<?php
namespace App\Model;
use PDO, PDOException;

class Database
{
    protected $dbh;
    public $conn;


    public $username="root";
    public $password="";
    public function __construct()
    {
        try {

            $this->dbh = new PDO("mysql:host=localhost;dbname=originative_coders", "root", "");
            $this->conn = new PDO("mysql:host=localhost;dbname=ums", $this->username, $this->password);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Database connection successful!<br>";

        }
        catch (PDOException $error){

            echo $error->getMessage();

        }


    }


}