<?php
if(!isset($_SESSION) )session_start();
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Profile</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="../../../resources/profiledesigns/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="../../../resources/profiledesigns/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="../../../resources/profiledesigns/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Acme' rel='stylesheet' type='text/css'><!-- //fonts -->

	<!-- start-smoth-scrolling -->
		<script type="text/javascript" src="../../../resources/profiledesigns/js/move-top.js"></script>
		<script type="text/javascript" src="../../../resources/profiledesigns/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
	<!-- start-smoth-scrolling -->

<!-- skills -->

<!-- skills -->
<script type="text/javascript" src="../../../resources/profiledesigns/js/numscroller-1.0.js"></script>

</head>
<body>
<!-- banner -->
<div class="header-top">
	<div class="container">
		<ul>
			<li><a class="scroll" href="#about"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>Resume</a></li>
			<li><a class="scroll" href="#emailme"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>Email me</a></li>
			<li><a href="javascript:window.print()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Print Profile</a></li>
			<li><a href="#portfolioModal9" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal">
				<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>My Photo</a></li>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		</ul>
	</div>
</div>


<div class="header">
    <table align="center">
        <tr>
            <td height="50" >

                <div id="message" >

                    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                        echo "&nbsp;".Message::message();
                    }
                    Message::message(NULL);

                    ?>

                </div>

            </td>
        </tr>
    </table>
	<div class="container">
		<div class="col-md-8 header-left">
			<div class="col-sm-5 pro-pic">
                <img class="img-responsive" src="../../../resources/profiledesigns/images/pic1.jpg" alt=" "/>
			</div>
			<div class="col-sm-5 pro-text">
				<h1><?php echo "$singleUser->first_name $singleUser->last_name"?></h1>

			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-4 header-right ">
			<ul class="list-left">
				<li>Email:</li>
				<li>Website:</li>
				<li>Phone no:</li>
				<li>Address: </li>
			</ul>
			<ul class="list-right">
				<li><a href="mailto:info@example.com"><?php echo "$singleUser->email"?></a></li>
				<li><a href="mailto:info@example.com">info@example.com</a></li>
				<li><?php echo "$singleUser->phone"?></li>
				<li><?php echo "$singleUser->address"?></li>
			</ul>
			<div class="clearfix">
                <nav>
                    <a class="btn btn-danger" href= "User/Authentication/logout.php"> LOGOUT </a>

                </nav>
            </div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //banner -->
<a href="../../../view/OriginativeCoders/Mess/create.php" class="btn btn-default">Post a ad</a>

	<!--scrolling js-->
	<script src="../../../resources/profiledesigns/js/jquery.nicescroll.js"></script>
	<script src="../../../resources/profiledesigns/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->

<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>

</body>
</html>
