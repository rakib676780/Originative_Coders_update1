<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Utility\Utility;
use App\Mess\Mess;


$obj = new Mess();
$allData  =  $obj->index();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Hostels Online </title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/normalize.css">
    <link rel="stylesheet" href="../../../resources/style.css">
</head>
<body style="background-color: rgba(97,112,120,0.73)">

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<!--Navigation Starts-->
<div>
    <nav class="navbar navbar-default navbar-fixed-top mynav" style="background-color: black">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../../../index.php" style="margin-left: 40px;'"><b style="font-size: 55px; color: #985f0d;margin-top: 3px">H</b>OSTELS ONLINE</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php"> HOME  <span class="sr-only">(current)</span></a></li>
                    <li><a href=""> RESERVATION S</a></li>
                    <li><a href=""> FEATURES </a></li>
                    <li><a href=""> ABOUT US</a></li>
                    <li><a href=""> CONTACT US </a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    <nav>
</div>

<!--Navigation Ends-->
<div class="container">
    <div class="hostel-info-area" style="background-color: transparent">


        <!--------------index start--------->

        <a class="btn btn-primary" href="create.php"> Post </a>
        <table class="table table-striped" style="opacity: 0.9; margin-top: 4px">

            <tr>

                <th> Serial </th>
                <th> ID </th>
                <th> Post_Id </th>
                <th> User_Name </th>
                <th> Building_Name </th>
                <th> Location </th>
                <th> District </th>
                <th> Total_Sit </th>
                <th> Room_Number </th>
                <th> Mobile_Number </th>
                <th> Gender </th>
                <th> Condition </th>


            </tr >

            <form  id="multiple" method="post">

                <?php


                $serial=1;

                foreach ($allData as $oneData){

                    if($serial%2) $bgColor = "rgba(226,226,213,0.73)";
                    else $bgColor = "rgba(226,226,213,0.73)";

                    echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     <td style='width: 10%; text-align: center'>$serial</td>
                                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                                     <td style='width: 10%; text-align: center'>$oneData->post_id</td>
                                     <td style='width: 10%; text-align: center'>$oneData->user_name</td>
                                     <td style='width: 20%;'>$oneData->building_name</td>
                                     <td style='width: 40%;'>$oneData->location</td>
                                     <td style='width: 40%;'>$oneData->district</td>
                                     <td style='width: 40%;'>$oneData->sit_available</td>
                                     <td style='width: 40%;'>$oneData->room_number</td>
                                     <td style='width: 40%;'>$oneData->mobile_number</td>
                                     <td style='width: 40%;'>$oneData->typ</td>
                                     <td style='width: 40%;'>$oneData->conditi</td>
                    
   
                                  </tr>
                              ";
                    $serial++;

                }

                ?>
        </table>
        </form>

        <!-------index end------>

    </div>



</div>


</body>
</html>