-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2017 at 06:00 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `originative_coders`
--
CREATE DATABASE IF NOT EXISTS `originative_coders` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `originative_coders`;

-- --------------------------------------------------------

--
-- Table structure for table `hostels`
--

CREATE TABLE `hostels` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `addr` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `t_seats` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `a_seats` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pic` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hostels`
--

INSERT INTO `hostels` (`id`, `name`, `addr`, `phone`, `type`, `t_seats`, `a_seats`, `pic`) VALUES
(1, 'mm', 'jhakjs', '0000000', 'Nresidential', '', '', ''),
(2, 'new', 'hjjka nsns', '0189990099', 'Nresidential', '', '', ''),
(3, 'new', 'hjhkjhk', '01999299', 'Nresidential', '', '', '1507266522'),
(4, 'mkl', 'hjk', '019992993', 'Nresidential', '', '', '1507266617'),
(5, 'nnn', 'hjjjjjjjhh', '12333333', 'residential', '', '', '1507268505'),
(6, 'nnn', 'hjjjjjjjhh', '12333333', 'residential', '', '', '1507269234'),
(7, 'nnn', 'hjjjjjjjhh', '12333333', 'Nresidential', '', '', '1507269753'),
(8, 'nnn', 'hjjjjjjjhh', '12333333', 'Nresidential', '', '', '1507269835'),
(9, 'nnn', 'hjjjjjjjhh', '12333333', 'Nresidential', '', '', '15072708906.jpg'),
(10, 'gg', 'gggggggggggggggggggggg', '1111111111', 'residential', '1233', '123', '15072719056.jpg'),
(11, 'gg', 'gggggggggggggggggggggg', '1111111111', 'residential', '1233', '123', '15072719222.jpg'),
(12, 'chatrabash', 'Chittagong', '112344433', 'residential', '123', '3', '1507301190Screenshot (1).png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(32, 'Tushar', 'Chowdhury', 'tushar.chowdhury@gmail.com', '71f55003c9a36b40c4a094908f11fb77', '1327487282', 'gfddhfdty', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hostels`
--
ALTER TABLE `hostels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hostels`
--
ALTER TABLE `hostels`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
